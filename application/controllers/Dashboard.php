<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		//$this->load->library("session");
		if($this->session->has_userdata('user')){
		$this->load->view('dashboard_view');
	}else{
		$this->load->view('welcome_message');
	}
	}

	
}
